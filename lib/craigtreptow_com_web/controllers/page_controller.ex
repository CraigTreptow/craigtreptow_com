defmodule CraigtreptowComWeb.PageController do
  use CraigtreptowComWeb, :controller

  def index(conn, _params), do: render(conn, "index.html")

  def about(conn, _params), do: render(conn, "about.html")

  def learning(conn, _params), do: render(conn, "learning.html")
  def resume(conn, _params), do: render(conn, "resume.html")
  def projects(conn, _params), do: render(conn, "projects.html")
  def social(conn, _params), do: render(conn, "social.html")
end
